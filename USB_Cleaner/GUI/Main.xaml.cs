﻿using System.IO;
using System.Windows;
using System.Collections.Generic;

using Protego.Logic;

namespace Protego.GUI {
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window {

        #region Properties
        //контроллер устройств и файлов
#pragma warning disable IDE1006 // Стили именования
        private IOController controller { get; set; }
#pragma warning restore IDE1006 // Стили именования
        //выбранное из списка устройства
        private DriveInfo CurrentDrive { get; set; }
        //сохранены ли настройки

        #endregion

        #region Contructors
        public Main() {
            //InitializeComponent();
            Init();
            InitGUI();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Инициализация
        /// </summary>
        void Init() {
            //создаем объект контроллера
            controller = new IOController();
        }

        /// <summary>
        /// Инициализация графичекого интерфейса
        /// </summary>
        void InitGUI() {
            //назначаем контекст для привязки данных
            // gridSettings.DataContext = sets;
            DataContext = controller;
            //включаем панель выбора устройств
            ShowDevicesSection(null, null);
        }
        #endregion

        #region Window actions
#pragma warning disable IDE1006 // Стили именования
        private void helpClick(object sender, System.Windows.Input.MouseButtonEventArgs e) {
#pragma warning restore IDE1006 // Стили именования
            //выводим окно о программе
            new About().ShowDialog();
        }


        private void FormClosing(object sender, System.ComponentModel.CancelEventArgs e) {

        }

#pragma warning disable IDE1006 // Стили именования
        private void comboDeviceSelected(object sender, System.Windows.Controls.SelectionChangedEventArgs e) {
#pragma warning restore IDE1006 // Стили именования
            //при выборе устройства включаем кнопку "перейти к очистке"
            //buttonToClean.IsEnabled = true;
            //запоминаем выбранное устройство
            CurrentDrive = (DriveInfo)comboDevices.SelectedItem;
        }

        private void ShowDevicesSection(object sender, RoutedEventArgs e) {
            //меняем видимость панелей
            //gridCleaner.Visibility = gridSettings.Visibility = Visibility.Hidden;
            //gridDevices.Visibility = Visibility.Visible;
        }

        private void ShowCleanerSection(object sender, RoutedEventArgs e) {
  
        }
        #endregion

        private void UpdateDeviceList(object sender, RoutedEventArgs e) {
            //обновляем список устройств
            controller.Drives = controller.GetUSBDrives();
        }

        private void DeleteAll(object sender, RoutedEventArgs e) {
              //если выбранно устройство
            if (CurrentDrive != null) {
                if(System.Windows.Forms.MessageBox.Show(
                    "Очистка выбранного устройства, продолжить?",
                    "Предупреждение",
                    System.Windows.Forms.MessageBoxButtons.YesNo,
                    System.Windows.Forms.MessageBoxIcon.Warning
                ) == System.Windows.Forms.DialogResult.Yes)
                {
                    //очищаем устройство
                    controller.DeleteAll(CurrentDrive);
                    System.Windows.Forms.MessageBox.Show("Вроде все очищено");
                }
            } else {
                //иначе выводим сообщение
                System.Windows.Forms.MessageBox.Show("Сначала выберите устройство!");
            }
        }


#pragma warning disable IDE1006 // Стили именования
        private void closeWindow(object sender, RoutedEventArgs e) {
#pragma warning restore IDE1006 // Стили именования
            //закрываем окно
            Close();
        }
 
    }
}
