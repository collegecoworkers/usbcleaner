﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Protego.Logic {

    public class IOController : INotifyPropertyChanged {

        #region Properties Event Logic
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPopertyChanged(string propertyName) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Properties
        //коллекции устройств и файлов
        private List<DriveInfo> _drives;
        private ObservableCollection<FileInfo> _files;

        /// <summary>
        /// Список внешних устройств
        /// </summary>
        public List<DriveInfo> Drives {
            get { return _drives; }
            set {
                if (_drives != value)
                    _drives = value; OnPopertyChanged("Drives");
            }
        }

        /// <summary>
        /// Список файлов
        /// </summary>
        public ObservableCollection<FileInfo> DangerFiles {
            get { return _files; }
            set {
                if (_files != value)
                    _files = value; OnPopertyChanged("DangerFiles");
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Инициализация контроллера устройств и файлов
        /// </summary>
        public IOController() {
            //инициализируем коллекции носителей и файлов
            Drives = new List<DriveInfo>();
            DangerFiles = new ObservableCollection<FileInfo>();

            //получаем список внешних устройств
            Drives = GetUSBDrives();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Метод для получения списка устройств с типом Removable
        /// </summary>
        /// <returns>Коллекция DriveInfo</returns>
        public List<DriveInfo> GetUSBDrives() {
            //создаем временную коллекцию
            List<DriveInfo> temp = new List<DriveInfo>();

            //циклом для всех устройств отбираем с типом Removable
            foreach (var drive in DriveInfo.GetDrives()) {
                //и добавляем во временную коллекцию
                if (drive.DriveType == DriveType.Removable) temp.Add(drive);
            }
            //возвращаем коллекцию как результат
            return temp;
        }

        /// <summary>
        /// Удалить все файлы
        /// </summary>
        public void DeleteAll(DriveInfo drive) {
             //если устройство передано
            if (drive != null) {
                foreach (var file in drive.RootDirectory.GetFiles()) {
                    file.Delete();
                }
                foreach (var subDir in drive.RootDirectory.GetDirectories()) {
                    subDir.Delete(true);
                }
            }
        }
    #endregion
    }
}
